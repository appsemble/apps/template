BEGIN;
INSERT INTO "User" (
    id,
    name,
    "primaryEmail",
    password,
    timezone,
    created,
    updated
  )
VALUES (
    '7d8a3162-a331-4fea-a0d8-312d0be973f0',
    'Appsemble Bot',
    'bot@appsemble.com',
    '$2b$10$1KeSoiRlya.x5s0IteVFCukqLVuZwCniMDK6kE9H653RQaZ3RDKAW',
    'Europe/Amsterdam',
    NOW(),
    NOW()
  );
INSERT INTO "EmailAuthorization" (
    email,
    verified,
    created,
    updated,
    "UserId"
  )
VALUES (
    'bot@appsemble.com',
    true,
    NOW(),
    NOW(),
    '7d8a3162-a331-4fea-a0d8-312d0be973f0'
  );
INSERT INTO "OAuth2ClientCredentials" (
    id,
    secret,
    description,
    scopes,
    created,
    "UserId"
  )
VALUES (
    'test',
    '$2b$10$1KeSoiRlya.x5s0IteVFCukqLVuZwCniMDK6kE9H653RQaZ3RDKAW',
    'Used for provisioning the review environment',
    'apps:write resources:write assets:write blocks:write organizations:write teams:write',
    NOW(),
    '7d8a3162-a331-4fea-a0d8-312d0be973f0'
  );
COMMIT;
