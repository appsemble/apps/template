# E2E Tests

Start containers to use to write tests:

```sh
docker compose up -d
```

Setup Appsemble with the required organizations and app(s).

```sh
npx appsemble organization create appsemble --name 'Appsemble'  --context development --client-credentials 'test:test' --remote 'http://localhost:9999'
npx appsemble organization create template --name 'Template' --context development --client-credentials 'test:test' --remote 'http://localhost:9999'
npx appsemble block publish blocks/* --client-credentials 'test:test' --remote 'http://localhost:9999'
npx appsemble app create apps/template --context development --client-credentials 'test:test' --remote 'http://localhost:9999'
```

Or you can set these values once in the config.

```sh
npx appsemble config set context review
npx appsemble config set remote "http://appsemble:9999"
npx appsemble config set client-credentials "test:test"
```

And then run

```sh
npx appsemble organization create appsemble --name 'Appsemble'
npx appsemble organization create template --name 'Template'
npx appsemble block publish blocks/*
npx appsemble app create apps/template
```

To run end to end tests, run:

```sh
npm run test
```
