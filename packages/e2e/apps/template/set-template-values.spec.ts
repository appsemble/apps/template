import { expect } from '@playwright/test';

import { test } from '../../fixtures/test/index.js';

test.describe('Admin flow', () => {
  test.beforeEach(async ({ appsemble }) => {
    await appsemble.visitApp();
    await appsemble.loginApp();
  });

  test('should create check', async ({ page }) => {
    await page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    await page.getByPlaceholder('Title').fill('Test Title');
    await page.getByPlaceholder('Content').fill('Test content...');

    await page.getByRole('button', { name: 'Change' }).click();

    await expect(page.getByRole('heading', { name: 'Title' })).toBeVisible();
    await expect(page.getByText('Test content...', { exact: true })).toBeVisible();
  });
});
