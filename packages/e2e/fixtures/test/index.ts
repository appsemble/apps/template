import { test as base, expect, type Page } from '@playwright/test';

const { BOT_ACCOUNT_EMAIL = 'bot@appsemble.com', BOT_ACCOUNT_PASSWORD = 'test' } = process.env;

interface Fixtures {
  /**
   * Visit an app.
   *
   * Uses to values from the Appsemble class.
   */
  visitApp: () => Promise<void>;

  /**
   * Set the role of the user.
   *
   * @param role The role to set.
   *
   *   Note that this requires the user to be logged in to the studio.
   *
   *   Uses to values from the Appsemble class.
   */
  changeRole: (role: string) => Promise<void>;

  /**
   * Login to the Appsemble studio.
   *
   * @param redirect The URL to navigate to after logging in.
   */
  studioLogin: (redirect: string) => Promise<void>;

  /**
   * Login to an Appsemble app.
   *
   * Uses to values from the Appsemble class.
   */
  loginApp: () => Promise<void>;
}

export class Appsemble implements Fixtures {
  readonly page: Page;

  readonly organization: string;

  readonly app: string;

  readonly host: string;

  constructor(page: Page, organization: string, app: string, host: string) {
    this.page = page;
    this.organization = organization;
    this.app = app;
    this.host = host;
  }

  async visitApp(): Promise<void> {
    await this.page.goto(`http://${this.app}.${this.organization}.${this.host}`);
  }

  async studioLogin(): Promise<void> {
    const queryParams = new URLSearchParams({ redirect: '/en/apps' });
    await this.page.goto(`/en/login?${String(queryParams)}`);

    await this.page.waitForLoadState('domcontentloaded');
    await this.page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    await this.page.getByTestId('email').fill(BOT_ACCOUNT_EMAIL);
    await this.page.getByTestId('password').fill(BOT_ACCOUNT_PASSWORD);
    await this.page.getByTestId('login').click();

    await expect(this.page).toHaveURL('/en/apps');
  }

  async changeRole(role: string): Promise<void> {
    const redirect = this.page.url();
    await this.page.goto(`/en/organizations/${this.organization}`);
    await this.page.click(`text=${this.app}`);
    const appId = this.page.url().split('/').pop();
    await this.page.goto(`/en/apps/${appId}/users`);
    const select = this.page.locator('tr', { hasText: 'It’s you!' }).locator('select[class=""]');
    await select.selectOption(role);
    await this.page.goto(redirect);
  }

  async loginApp(): Promise<void> {
    await this.page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    await this.page.getByTestId('login-with-appsemble').click();

    const emailInput = this.page.getByTestId('email');
    await this.page.waitForLoadState('domcontentloaded');
    await this.page.waitForSelector('.appsemble-loader', { state: 'hidden' });

    if (await emailInput.isVisible()) {
      await this.page.getByTestId('email').fill(BOT_ACCOUNT_EMAIL);
      await this.page.getByTestId('password').fill(BOT_ACCOUNT_PASSWORD);
      await this.page.getByTestId('login').click();

      const appId = new URL(this.page.url()).searchParams.get('client_id')!.split(':').pop()!;
      const response = await this.page.waitForResponse(
        `/api/users/current/auth/oauth2/apps/${appId}/consent/verify`,
      );
      if (response.ok()) {
        return;
      }
      const responseBody = await response.text();
      if (responseBody.includes('User has not agreed to the requested scopes')) {
        await this.page.getByTestId('allow').click();
        return;
      }
    }
    await this.page.waitForSelector('.appsemble-loader', { state: 'hidden' });
    const allowButton = this.page.getByTestId('allow');

    if (await allowButton.isVisible()) {
      await allowButton.click();
    }
  }
}

const test = base.extend<{ appsemble: Appsemble }>({
  async appsemble({ baseURL, page }, use) {
    await use(
      new Appsemble(
        page,
        'template',
        'template-app',
        new URL(baseURL ?? 'http://localhost:9999').host,
      ),
    );
  },
});

export { test };
