export {};

declare module '@appsemble/sdk' {
  interface EventListeners {
    /**
     * When received, the block will display the data.
     */
    refresh: never;
  }

  interface Parameters {
    /**
     * The title displayed in the template block.
     */
    title?: string;

    /**
     * The content displayed in the template block.
     */
    content?: string;
  }

  interface Messages {
    /**
     * The label that’s used when the title is missing.
     */
    title: never;

    /**
     * The label that’s used when the content is missing.
     */
    content: never;
  }
}
