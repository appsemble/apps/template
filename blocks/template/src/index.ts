import { bootstrap } from '@appsemble/sdk';

import './index.css';

bootstrap(({ events, utils }) => {
  const div = document.createElement('div');
  div.classList.add('container');

  const title = document.createElement('h2');
  title.textContent = 'Missing title...';
  const content = document.createElement('div');
  title.textContent = 'Missing content...';

  function loadData(data?: Record<string, unknown>): void {
    title.textContent = String(
      utils.remap({ prop: 'title' }, data) || utils.formatMessage('title'),
    );
    content.textContent = String(
      utils.remap({ prop: 'content' }, data) || utils.formatMessage('content'),
    );
  }

  events.on.refresh(loadData);

  loadData();

  div.append(title, content);

  return div;
});
