# Template

> This repository is the guide to setting up and maintaining your Appsemble apps in GitLab.

## Table of Contents

- [Getting started](#getting-started)
- [Project structure](#project-structure)
- [App development](#app-development)
- [Managing the GitLab project](#managing-the-gitlab-project)
  - [Contributing](#contributing)
  - [CI/CD](#cicd)
- [Developing custom blocks](#developing-custom-blocks)
- [E2E testing](#e2e-testing)
  - [Prerequisites](#prerequisites)
  - [Writing tests](#writing-tests)
  - [Enabling E2E tests in the CI/CD pipeline](#enabling-e2e-tests-in-the-cicd-pipeline)

### Getting started

To start using GitLab to maintain app(s) for an organization a GitLab project (repository) needs to
be created in the [Apps subgroup](https://gitlab.com/appsemble/apps), to create one ask any
[Maintainer or Owner](https://gitlab.com/groups/appsemble/apps/-/group_members?sort=access_level_desc)
to create one. It's best to name the GitLab project the same as the organization id.

If the project already exists, the next step is getting familiar with the
[Project structure](#project-structure).

### Project structure

In this repository a structure has been put together for maintaining app('s) in GitLab. If the
project is empty or only contains a README.md, the files in this repository can be copied over to
get started.

There are a few important things to know about the structure. Firstly, there is an `apps` folder
where all the applications of the organization should go. This repository contains a
[template app](apps/template/) app to use as reference. This reference can be used to see how to
structure an app in files and directories.

Then there is the `blocks` folder where custom blocks can be put that belong to the organization. To
read more about custom blocks and how to develop them see the
[Developing custom blocks](#developing-custom-blocks) section.

There is also a `packages` folder to house any custom code needed to manage a project. It already
has a package called `e2e`, which houses some configuration files for end-to-end testing and is also
the place to put end-to-end tests to make sure applications still work when deployed. Read more
about end-to-end testing in the [E2E testing](#e2e-testing) section.

```
my-repository/
  ├── apps/
  │   └── my-application/
  │       └── ..
  │
  ├── blocks/
  │   └── custom-block/
  │       └── ..
  │
  ├── packages/
  │   └── e2e/
  │
  ├── .gitlab-ci.yml
  ├── package-lock.json
  ├── package.json
  └── README.md
```

Lastly, some notes on the files shown in the example above.

- The `.gitlab-ci.yml` is used to configure pipelines, to check for code quality issues and to allow
  for deployments.
- The `package.json` contains some dependencies required for the code quality checks, but most
  noteably the Appsemble CLI package. It also contains 2 scripts to use for end-to-end testing.
- The `package-lock.json` will tell exactly what versions of dependencies are installed and normally
  shouldn't need to be updated. Altough sometimes new additions get added to the CLI package like
  the development server that could help speed up development.
- The `README.md` contents should be replaced preferably with some general information on the
  project and point out any custom configurations to look out for.

### App development

In order to develop apps locally the following should be installed.

- [Docker](https://docker.com)
- [Docker Compose](https://docs.docker.com/compose)
- [NodeJS 18](https://nodejs.org)
- [npm](https://docs.npmjs.com/cli/v9/configuring-npm/install)

When done the repository can be cloned and the required dependencies can be installed using
`npm ci`. Now the local environment should be ready to start developing and maintaining applications
locally.

To start developing apps the [appsemble.app](https://appsemble.app) studio can of course be used;
app-definitions, styles and translations can be copied from the local repository into the studio.
However, the main Appsemble repository can also be cloned and used to publish and update apps in
development mode.

The development server may also be used via the `npx appsemble serve apps/my-app` command, but this
is currently meant mostly to develop blocks with. In the future this will hopefully become more
usefull for app development as well as it currently requires a rerun of the command everytime a
change is made to the app-definition.

> Note: the development server has not yet been released in the CLI package.

### Managing the GitLab project

#### Contributing

Preferably follow the contribution guidelines as in the main
[Appsemble repository](https://gitlab.com/appsemble/appsemble/-/blob/main/CONTRIBUTING.md) where
possible.

#### CI/CD

The project uses `prettier`, `eslint`, `typescript` and an in-house translation validation script to
make sure app code follows Appsembles conventions. In the `.gitlab-ci.yml` file the jobs for these
checks can be found, which will run when a new merge request is created and future commits are made
to the same branch or in the main branch.

**Deploying**

When an app is ready and should be deployed to production, the first step is to make sure the app is
created under the correct organization at [appsemble.app](https://appsemble.app). Then the app-id
can copy be copied and set it in the apps' `.appsemblerc.yaml` file under production, also make sure
the organization id is set correctly.

```yaml
context:
  production:
    id: ...
    organization: ...
    remote: https://appsemble.app
    showAppDefinition: false
    visibility: unlisted
  ...
```

Then when this is setup the deploy job can be enabled in the `.gitlab-ci.yml` file, by removing
`when: never` from the job rules and asking for permission from one of the
[Owners](https://gitlab.com/groups/appsemble/apps/-/group_members?sort=access_level_desc) to setup
any secrets that are required for deployments.

### Developing custom blocks

Appsemble has official blocks, but sometimes they don't cover everything required by an app. That's
where custom blocks come in. The blocks folder contains an example of one.

The [Developing Blocks](https://appsemble.app/en/docs/02-development/02-developing-blocks) guide
will teach how to get started (to create the block use `npx create-appsemble block` instead). The
development server can be used by running `appsemble serve apps/my-app` command. It will
automatically update the block when the file is saved. For more information see the
[Appsemble CLI README](https://gitlab.com/appsemble/appsemble/-/blob/main/packages/cli/README.md).

> Note: the development server has not yet been released in the CLI package.

Alternatively, the regular Appsemble server can be used to develop blocks for more on this see
[Developing custom blocks](https://appsemble.app/en/docs/03-guide/custom-dependencies). _Using the
containers from the docker-compose.yml file should also work, so nothing needs to be moved._

### E2E testing

You can use the playwright docker container in the `docker-compose-e2e.yaml` file to run your tests.

Alternatively, you can run the following command and use playwright to run e2e tests locally:

```sh
npx playwright install --with-deps
```

See [install browsers](https://playwright.dev/docs/browsers#install-browsers) for more.

#### Writing tests

Writing end-to-end tests requires a local setup of Appsemble. The `docker-compose-e2e.yaml` file
will create the required containers. A test user will be created in the database container using the
[seed-account.ts](packages/scripts/commands/seed-account.ts) script. The user credentials are the
following:

- Email: `bot@email.com`
- Password: `password`
- Client-credentials: `user:password`

Use these credentials to login when writing end-to-end tests locally. The server should be available
on https://localhost:9999.

In order to ensure that your app will continue to work, you can configure its e2e tests to run
automatically before a new version of Appsemble gets released. You should add your app to the list
of apps in the main Appsemble pipeline in the `e2e apps` job by creating a merge request. It should
point to the app GitLab repository like this `organization-name/apps/app-name`.

For more see the [README](packages/e2e/README.md) in the e2e package.

**Tips**

- Use `npm run codegen` to walk through the app and record each step.
- Look at the videos produced in the test-results folder to troubleshoot.
- Install the official Playwright plugin for vscode, see
  [getting started with vscode](https://playwright.dev/docs/getting-started-vscode) (and enable show
  browser).
- Use the official Appsemble test fixtures such as in
  [packages/e2e/fixtures/test/index.ts](packages/e2e/fixtures/test/index.ts). _These will likely be
  moved to the Appsemble repository and released separately in the near future._
- If a test is failing because the app is still loading the following 2 lines can be used to make
  sure the UI is ready:

```typescript
await page.waitForLoadState('domcontentloaded');
await page.waitForSelector('.appsemble-loader', { state: 'hidden' });
```

- Please, follow the [Playwright best practices](https://playwright.dev/docs/best-practices) as much
  as possible.

#### Structure

The e2e package is structured with an `apps` and `fixtures` directory. The `apps` directory should
contain a folder for each app containing the tests related to it. The `fixtures` directory contains
reusable code in the test folder and assets in the data folder.

When the tests finish, a test-results folder is created containing a video for each individual test.

```
my-repository/
  ├── ...
  ├── packages/
  │   └── e2e/
  │       ├── apps/
  │       │   ├── template/
  │       │   └── ...
  │       ├── fixtures/
  │       │   ├── data/
  │       │   └── test/
  │       │       ├── ...
  │       │       └── ...
  │       ├── test-results
  │       └── playwright.config.ts
  └── ...
```

#### Enabling E2E tests in the CI/CD pipeline

To enable the tests, the test job should be configured to fit the project, by removing `when: never`
from the job rules in the `.gitlab-ci.yml` file and adjusting the configuration accordingly.

To troubleshoot failing tests, click on the test job to look for “Job artifacts” which should
contain videos of each of the ran tests.

#### Trigger E2E tests downstream from Appsemble

The E2E tests written for app repositories can be triggered each time a pipeline is created in the
Appsemble repository to test whether the app still works with new upstream Appsemble changes.

The Appsemble pipeline contains a job called
[`e2e apps`](https://gitlab.com/appsemble/appsemble/-/blob/71ccf9d86e7979269d380f2a0ae259e5cdfccfc3/.gitlab-ci.yml#L250),
this job must contain the path to the repository, such as `appsemble/apps/template`.

The downstream pipeline requires permissions from the downstream protected branch; the protected
branch in the app repository. To enable this go to settings at
`…/-/settings/repository#js-protected-branches-settings`, e.g.
<https://gitlab.com/appsemble/apps/template/-/settings/repository#js-protected-branches-settings>.

Here you can find the protected branches and rules. The default branch must have `Allowed to merge`
set to `Developers + Maintainers`, to allow `Developers` in the Appsemble organization
[to trigger the downstream pipeline](https://gitlab.com/appsemble/appsemble/-/issues/1359).
