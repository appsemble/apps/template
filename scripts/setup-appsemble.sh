npx appsemble organization create appsemble
npx appsemble organization create ${ORGANIZATION_ID:-template}

npx appsemble -vv block publish blocks/*
npx appsemble -vv app publish apps/* --modify-context
